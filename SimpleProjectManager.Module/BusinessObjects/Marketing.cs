﻿using System;
using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace SimpleProjectManager.Module.BusinessObjects.Marketing
{
    [NavigationItem("Marketing")]
    public class Customer : BaseObject
    {
        public Customer()
        {
        }
        public Customer(Session session) : base(session) { }
        string firstName;
        public string FirstName
        {
            get { return firstName; }
            set { SetPropertyValue(nameof(FirstName), ref firstName, value); }
        }
        string lastName;
        public string LastName
        {
            get { return lastName; }
            set { SetPropertyValue(nameof(LastName), ref lastName, value); }
        }
        string email;
        public string Email
        {
            get { return email; }
            set { SetPropertyValue(nameof(Email), ref email, value); }
        }
        string company;
        public string Company
        {
            get { return company; }
            set { SetPropertyValue(nameof(Company), ref company, value); }
        }
        string occupation;
        public string Occupation
        {
            get { return occupation; }
            set { SetPropertyValue(nameof(Occupation), ref occupation, value); }
        }
        XPCollection<Testimonial> testimonials;
        [Association("Customer-testimonial"), DevExpress.Xpo.Aggregated]
        public virtual XPCollection<Testimonial> Testimonials
        {
            get { return GetCollection<Testimonial>(nameof(Testimonials)); }
            set { SetPropertyValue(nameof(Testimonials), ref testimonials, value); }
        }
        [NonPersistent]
        public string FullName
        {
            get
            {
                string namePart = string.Format("{0} {1}", FirstName, LastName);
                return Company != null ? string.Format("{0} ({1})", namePart, Company) : namePart;
            }
        }
        byte[] photo;
        [ImageEditor(ListViewImageEditorCustomHeight = 75, DetailViewImageEditorFixedHeight = 150)]
        public byte[] Photo
        {
            get { return photo; }
            set { SetPropertyValue(nameof(Photo), ref photo, value); }
        }
    }
    [NavigationItem("Marketing")]
    public class Testimonial : BaseObject
    {
        public Testimonial(Session session) : base(session) { }
        public Testimonial()
        {
            createdOn = DateTime.Now;
        }
        string quote;
        [Size(SizeAttribute.Unlimited)]
        public string Quote
        {
            get { return quote; }
            set { SetPropertyValue(nameof(Quote), ref quote, value); }
        }
        string highlight;
        [FieldSize(512)]
        public string Highlight
        {
            get { return highlight; }
            set { SetPropertyValue(nameof(Highlight), ref highlight, value); }
        }
        DateTime createdOn;
        [VisibleInListView(false)]
        public DateTime CreatedOn
        {
            get { return createdOn; }
            internal set { SetPropertyValue(nameof(CreatedOn), ref createdOn, value); }
        }
        string tags;
        public string Tags
        {
            get { return tags; }
            set { SetPropertyValue(nameof(Tags), ref tags, value); }
        }
        Customer customer;
        [Association("Customer-testimonial")]
        public virtual Customer AssociatedCustomer
        {
            get { return customer; }
            set { SetPropertyValue(nameof(AssociatedCustomer), ref customer, value); }
        }
    }
}